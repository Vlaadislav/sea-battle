﻿using System;

namespace Try_All_Logic
{
    class Program
    {
        public enum Direction
        {
            down = 1,
            right
        }
        // O - пустая клетка
        // * - холостой выстрел 
        // X - попал
        // # - корабль
        static void Main(string[] args)
        {

            char[,] array = new char[10, 10];
            CreateNewMap(array);
            ShawMap(array);
            Console.WriteLine();
            UserInterface(array);

            ShawMap(array);
        }
        public static void RandomPutTheShipOnTheMap(char[,] map)
        {
            Direction directionEnum;
            Random random = new Random();
            for (int deckCount = 4; deckCount > 0; deckCount--)
            {
                int index = 1;
                for (int shipCount = 0; shipCount < index; shipCount++)
                {
                    int randomXCoordinate = random.Next(10);
                    int randomYCoordinate = random.Next(10);
                    int randomDirection = random.Next(2);
                    Console.WriteLine("Введите в какую сторону вы хотите расположить корабль \n1 - вниз\n2 - вправо");
                    if (int.TryParse(Console.ReadLine(), out int direction) && direction > 0 && direction < 3)
                    {
                        if (direction == 1) directionEnum = Direction.down;
                        else directionEnum = Direction.right;
                        Console.WriteLine($"Введите координату для {deckCount} палубного коробля, например A:4 или B:3");
                        PutTheShipOnTheMap(map, deckCount, (randomXCoordinate, randomYCoordinate), directionEnum);
                        ShawMap(map);
                    }
                    else
                    {
                        Console.WriteLine("Введены некорректные значение");
                    }
                }
                index++;
            }


        }
        public static bool II(char[,] map)
        {
            Random random = new Random();
            int randomXCoordinate = random.Next(10);
            int randomYCoordinate = random.Next(10);
            return CanShotNextGamer(map, (randomYCoordinate, randomXCoordinate));
        }
        public static void UserInterface(char[,] map)
        {
            Direction directionEnum;
            for (int deckCount = 4; deckCount > 0; deckCount--)
            {
                int index = 1;
                for (int shipCount = 0; shipCount < index; shipCount++)
                {
                    Console.WriteLine("Введите в какую сторону вы хотите расположить корабль \n1 - вниз\n2 - вправо");
                    if (int.TryParse(Console.ReadLine(), out int direction) && direction > 0 && direction < 3)
                    {
                        if (direction == 1) directionEnum = Direction.down;
                        else directionEnum = Direction.right;
                        Console.WriteLine($"Введите координату для {deckCount} палубного коробля, например A:4 или B:3");
                        PutTheShipOnTheMap(map, deckCount, ParserCoordinats(EnterCoordicates()), directionEnum);
                        ShawMap(map);
                    }
                    else
                    {
                        Console.WriteLine("Введены некорректные значение");
                    }
                }
                index++;
            }

        }
        public static bool CanShotNextGamer(char[,] map, (int, int) coordinats)
        {

            if (map[coordinats.Item1, coordinats.Item2] != 'О' || map[coordinats.Item1, coordinats.Item2] != '#')
            {
                return false;
            }
            else
            {
                if (map[coordinats.Item1, coordinats.Item2] == 'О') map[coordinats.Item1, coordinats.Item2] = '*';
                else map[coordinats.Item1, coordinats.Item2] = '#';
                return true;
            }
        }
        public static void CreateNewMap(char[,] map)
        {
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    map[i, j] = 'О';
                }
            }
        }

        public static void ShawMap(char[,] map)
        {
            Console.Write("    ");
            for (int i = 0; i < map.GetLength(0); i++)
            {
                Console.Write(" " + (char)(65 + i));
            }
            Console.WriteLine("\n");
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (j == 0) Console.Write($"{(i + 1),2} |");
                    Console.Write(" " + map[i, j]);
                }
                Console.WriteLine();
                Console.WriteLine();
            }
        }



        public static string[] EnterCoordicates()
        {
            string enteredCoordinats = Console.ReadLine();
            enteredCoordinats = enteredCoordinats.Replace(" ", "");
            string[] coordinats = enteredCoordinats.Split(':');
            if (coordinats.Length != 2)
            {
                Console.WriteLine("такой координаты не существует");
            }
            if (!int.TryParse(coordinats[1], out int xCoordinate) && xCoordinate < 11 && xCoordinate > 0) Console.WriteLine("Вы ввели координаты неправильно");

            if (coordinats[0].Length != 1) Console.WriteLine("Вы ввели координаты неправильно");

            return coordinats;
        }

        public static (int, int) ParserCoordinats(string[] coordinats)
        {
            int xCoordinate = int.Parse(coordinats[1]);
            int yCoordinate = 0;
            switch (coordinats[0])
            {
                case "A":
                case "a":
                    yCoordinate = 1;
                    break;

                case "B":
                case "b":
                    yCoordinate = 2;
                    break;

                case "C":
                case "c":
                    yCoordinate = 3;
                    break;

                case "D":
                case "d":
                    yCoordinate = 4;
                    break;

                case "E":
                case "e":
                    yCoordinate = 5;
                    break;

                case "F":
                case "f":
                    yCoordinate = 6;
                    break;

                case "G":
                case "g":
                    yCoordinate = 7;
                    break;

                case "H":
                case "h":
                    yCoordinate = 8;
                    break;

                case "I":
                case "i":
                    yCoordinate = 9;
                    break;

                case "J":
                case "j":
                    yCoordinate = 10;
                    break;
                default:
                    yCoordinate = 0;
                    break;
            }
            return (xCoordinate, yCoordinate);
        }

        public static bool PutTheShipOnTheMap(char[,] map, int deckCount, (int, int) coordinats, Direction direction)
        {
            bool CanPutShip = true;
            try
            {
                switch (direction)
                {


                    case Direction.down:
                        for (int i = coordinats.Item1; i < coordinats.Item1 + deckCount; i++)
                        {
                            if (map[i, coordinats.Item2] != 'О'
                                && map[i + 1, coordinats.Item2] != '*' && map[i - 1, coordinats.Item2] != '*' && map[i, coordinats.Item2 + 1] != '*'
                                && map[i + 1, coordinats.Item2 + 1] != '*' && map[i - 1, coordinats.Item2 + 1] != '*')
                            {
                                CanPutShip = false;
                            }
                            else
                                map[i - 1, coordinats.Item2 - 1] = '#';
                        }
                        break;



                    case Direction.right:
                        for (int i = coordinats.Item2; i < coordinats.Item2 + deckCount; i++)
                        {
                            if (map[coordinats.Item1, i] != 'О'
                                && map[coordinats.Item1, i + 1] != '*' && map[coordinats.Item1 - 1, i] != '*' && map[coordinats.Item1 + 1, i] != '*'
                                && map[coordinats.Item1 + 1, i + 1] != '*' && map[coordinats.Item1 - 1, i + 1] != '*')
                            {
                                CanPutShip = false;
                            }
                            else
                                map[coordinats.Item1 - 1, i - 1] = '#';
                        }
                        break;
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return CanPutShip;
        }
    }
}

